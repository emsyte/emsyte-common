export enum ActivityEventType {
  REGISTRATION = "registration",
  PAID_REGISTRATION = "paid_registration",
  OFFER_CLICK = "offer_click",
  OFFER_PURCHASE = "offer_purchase",
  WATCHED_WEBINAR = "watched_webinar",
}
interface BaseActivityEvent {
  when: string;
}

export interface ActivityEventRegistered extends BaseActivityEvent {
  type: ActivityEventType.REGISTRATION;
  webinarName: string;
  webinarId: number;
}

export interface ActivityEventOfferWatchedWebinar extends BaseActivityEvent {
  type: ActivityEventType.WATCHED_WEBINAR;
  webinarName: string;
  webinarId: number;
  duration: number;
  percent_duration: number;
}

export interface ActivityEventPaidRegistration extends BaseActivityEvent {
  type: ActivityEventType.PAID_REGISTRATION;
  webinarName: string;
  webinarId: number;
  price: number;
}

export interface ActivityEventOfferClick extends BaseActivityEvent {
  type: ActivityEventType.OFFER_CLICK;
  offerName: string;
  offerId: number;
}

export interface ActivityEventOfferPurchase extends BaseActivityEvent {
  type: ActivityEventType.OFFER_PURCHASE;
  offerName: string;
  offerId: number;
  price: number;
}

export type ActivityEvent =
  | ActivityEventRegistered
  | ActivityEventOfferWatchedWebinar
  | ActivityEventPaidRegistration
  | ActivityEventOfferClick
  | ActivityEventOfferPurchase;
