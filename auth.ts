import { AttendeeObject } from "./attendee";
import { UserObject } from "./user";

export enum AuthType {
  USER = "user",
  ATTENDEE = "attendee",
}

export interface AuthAttendeeObject extends AttendeeObject {
  type: AuthType.ATTENDEE;
}

export interface AuthUserObject extends UserObject {
  type: AuthType.USER;
}

export interface APIAuthResponse {
  token: string;
  refreshToken: string;
  defaultTeam: string;
}
