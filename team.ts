import { UserObject } from "./user";

export const TEAM_SLUG_PATTERN = /^[A-Za-z0-9_-]+$/;

export enum TeamStatus {
  /** Billing is enabled, so the team can function properly */
  ENABLED = "enabled",

  /** Billing isn't setup, so the team functions are disabled */
  DISABLED = "disabled",
}

export enum TeamPlan {
  FREE = "free",
  BASIC = "basic",
  PREMIUM = "premium",
}

export enum TeamUserRole {
  OWNER = "owner",
  ADMIN = "admin",
  MEMBER = "member",
}

export enum TeamUserStatus {
  INVITED = "invited",
  ACTIVATED = "activated",
}

export interface TeamObject {
  name: string;
  slug: string;
  plan: TeamPlan;

  status: TeamStatus;

  // TODO: Should this only be visible to owners?
  billingEmail: string;
}

/**
 * A team that is available to the user
 */
export interface UserTeamObject {
  team: TeamObject;
  role: TeamUserRole;
  status: TeamUserStatus;
}

/**
 * A user that's on a team
 */
export interface TeamUserObject {
  user: UserObject;
  role: TeamUserRole;
  status: TeamUserStatus;
}

/*************
 * API Types *
 *************/

export interface APICheckTeamSlugQuery {
  slug: string;
}

export interface APICheckTeamResponse {
  message: string;
}

export interface APIRegisterTeamData {
  name: string;

  // Used to setup billing
  price: string;
  source: string;
  coupon?: string;
}

export interface APIEditTeamData extends Pick<TeamObject, "name"> {}

export interface APIInviteTeamMemberData {
  email: string;
  role: TeamUserRole;
}

export interface APIEditTeamMemberData {
  role: TeamUserRole;
}
