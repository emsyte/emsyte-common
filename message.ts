import { WebinarChatLabel } from "./webinar";

export enum LabelType {
  PRESENTER = "presenter",
  SUPPORT = "support_team",
  ATTENDEE = "attendee",
}

export enum MessageEventType {
  ATTENDEES_LIST = "attendeesList",
  BARS_CHANNEL = "barsChannel",
  HISTORY = "history",
  MESSAGE = "message",
  REMOVE = "remove",
  JOINED = "joinedChannel",
  LEFT = "leftChannel",
  ERROR = "error",
}

export type MessageType = {
  userId: string;
  messageId: string;
  content: string;
  name: string;
  label: LabelType;
  question?: boolean;
  removed?: WebinarChatLabel;
  ownMessage?: boolean;
  replyTo?: ReplyType;
};

export type ReplyType = {
  messageId: string;
  content: string;
  name: string;
  label: LabelType;
};

export interface BaseChatEvent {
  event: MessageEventType;
}

export interface ErrorEvent extends BaseChatEvent {
  event: MessageEventType.ERROR;
  content: string;
  channelId?: string;
}
export interface HistoryEvent extends BaseChatEvent {
  event: MessageEventType.HISTORY;
  messages: MessageType[];
  roomId: string;
  isAtBeginning: boolean;
  isAtEnd: boolean;
}

export interface ChatUser {
  label: LabelType;
  name: string;
  userId: string;
}

export interface AttendeesListEvent extends BaseChatEvent {
  event: MessageEventType.ATTENDEES_LIST;
  attendees: ChatUser[];
}

export interface FrequencyBar {
  question: boolean;
  count: number;
}
export interface BarsChannelEvent extends BaseChatEvent {
  event: MessageEventType.BARS_CHANNEL;
  bars: FrequencyBar[];
}

export interface RemoveEvent extends BaseChatEvent {
  event: MessageEventType.REMOVE;
  messageId: string;
  removedBy: string;
}

export interface JoinedEvent extends BaseChatEvent {
  event: MessageEventType.JOINED;
  user: ChatUser;
}

export interface LeftEvent extends BaseChatEvent {
  event: MessageEventType.LEFT;
  user: ChatUser;
}

export interface MessageEvent extends BaseChatEvent {
  event: MessageEventType.MESSAGE;
  roomId: string;
  token?: string;
  message: MessageType;
}

export type ChatEvent =
  | ErrorEvent
  | HistoryEvent
  | AttendeesListEvent
  | BarsChannelEvent
  | RemoveEvent
  | MessageEvent
  | JoinedEvent
  | LeftEvent;
