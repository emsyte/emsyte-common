export enum UserStatus {
  INACTIVE = "inactive",
  ACTIVE = "active",
}

export interface UserObject {
  id: number;
  firstName: string;
  lastName: string;
  status: UserStatus;
  email: string;
}

/*************
 * API Types *
 *************/

/** Response from an initial auth request (login, register) */
export interface APIAuthResponse {
  token: string;
  refreshToken: string;
  defaultTeam: string;
}

export interface APIAuthRegisterData {
  firstName: string;
  lastName: string;
  email: string;
  password: string;

  /** Name of the team to create */
  teamName: string;

  /** Stripe plan price */
  price: string;

  /** Stripe source token */
  source: string;

  /** Optional stripe coupon */
  coupon?: string;
}

export interface APIAuthLoginData {
  email: string;
  password: string;
}

export interface APIAuthRefreshTokenData {
  token: string;
  refreshToken: string;
}

/** API response for non-login/register actions */
export interface APIUserResponse {
  token: string;
  refreshToken: string;
}

export interface APIUserUpdateData {
  firstName: string;
  lastName: string;
  email: string;
}

export interface APIChangePasswordData {
  password: string;
  newPassword: string;
}

export interface APIUserForgotPasswordData {
  email: string;
}

export interface APIUserForgotPasswordConfirmData {
  password: string;
  token: string;
}
