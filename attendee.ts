import { ViewerEventObject } from "./webinar";

export interface AttendeeObject {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  active: boolean;
  timezone: string;

  createdDate: string;
  updatedDate: string;
}

export interface AttendeeObjectFull extends AttendeeObject {
  event: ViewerEventObject;
}
