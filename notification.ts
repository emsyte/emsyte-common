export enum NotificationType {
  /** As soon as an audience member registers */
  WELCOME = "welcome",

  /** A specified number of hours before the event starts */
  PRE_EVENT = "pre_event",

  /** A specified number of hours after the event ends */
  POST_EVENT = "post_event",

  /** 15 minutes before the webinar starts */
  LAST_MINUTE = "last_minute",
}

export enum NotificationDeliveryMethod {
  SMS = "sms",
  EMAIL = "email",
}

export interface NotificationObject {
  id: number;

  /** The notification kind determines when it should be sent */
  type: NotificationType;

  /** How the notification should be delivered (email or sms) */
  deliveryMethod: NotificationDeliveryMethod;

  /** Email subject line (ignored for sms) */
  subject: string;

  /** Content of the notification template */
  content: string;

  /**
   * When to send the notification.
   *
   * For pre-event notifications it's the number of hours before the event starts.
   * For post-event notifications it's the number of hours after the event ends.
   */
  hoursOffset: number;
}

export interface APINotificationCreateData
  extends Omit<NotificationObject, "id"> {}

export interface APINotificationUpdateData
  extends Omit<APINotificationCreateData, "type" | "deliveryMethod"> {}
