import { IntegrationObject, IntegrationProvider } from "./integration";

export enum WebinarChatLabel {
  ATTENDEE = "attendee",
  PRESENTER = "presenter",
  SUPPORT_TEAM = "support_team",
}

export enum WebinarType {
  /** Live webinar */
  LIVE = "live",

  /** Recorded webinar */
  RECORDED = "recorded",
}

export enum WebinarStatus {
  /** Webinar is in draft mode */
  DRAFT = "draft",

  /** Webinar is active */
  ACTIVE = "active",

  /** Webinar has been archived */
  ARCHIVE = "archive",
}

export enum AttendeeCounterType {
  FIXED = "fixed",
  DYNAMIC = "dynamic",
}

export enum Frequency {
  WEEKLY = "weekly",
  DAILY = "daily",
}

export interface NotificationGatewayObject {
  /** Which integration to use */
  integration: IntegrationObject;

  /** Settings for this particular gateway */
  settings: any;
}

/**
 * JSON representation of a webinar
 */
export interface WebinarObject {
  id: number;
  status: WebinarStatus;
  name: string;
  type: WebinarType;
  description: string;
  videoUrl: string;
  duration: number;
  replayExpiration: number;
  thumbnail?: string;
  isPaid: boolean;
  registrationFee: number;
  checkoutLink: string;
  redirectLink: string;

  settings: WebinarSettingsObject;

  emailGateway?: NotificationGatewayObject;
  smsGateway?: NotificationGatewayObject;
}

export interface PublicWebinarObject {
  id: number;
  name: string;
  type: WebinarType;
  description: string;
  thumbnail?: string;
  isPaid: boolean;
  registrationFee: number;
  checkoutLink: string;
  redirectLink: string;

  settings: PublicWebinarSettingsObject;
}

export interface WebinarSettingsObject {
  postRegistrationUrl: string;
  postViewUrl: string;

  registrationPageScript: string;
  thankyouPageScript: string;
  registrationFormScript: string;
  liveRoomScript: string;
  replayRoomScript: string;
}

export interface PublicWebinarSettingsObject {
  registrationFormScript: string;
}

/**
 * Webinar event
 */
export interface EventObject {
  id: number;
  start: string;
  end: string;
  recurrenceId: number;
  videoUrl: string;
  canceled: boolean;
}

export interface ViewerEventObject extends EventObject {
  webinar: Omit<WebinarObject, "videoUrl">;
}

export interface CalendarEventObject extends EventObject {
  webinar: WebinarObject;

  registered: number;
  attending: number;
}

/**
 * Webinar Offer
 */
export interface WebinarOfferObject {
  id: number;

  name: string;

  /** Start time in seconds since the start of the webinar */
  start: number;

  /** End time in seconds since the start of the webinar */
  end: number;

  /** Price of offer in cents */
  price: number;

  /** Description of the offer */
  description: string;

  /** Label inside the product offer button */
  buttonText: string;

  /** URL to product offer checkout page */
  checkout: string;

  /** URL to thumbnail file */
  thumbnail: string;
}

/**
 * Webinar saved chat message
 */
export interface WebinarChatObject {
  id: number;

  /** Seconds since the start of the webinar when the chat message should appear */
  offset_timestamp: number;

  /** The label on the chat message */
  label: WebinarChatLabel;

  /** The name that should appear on the chat message */
  name: string;

  /** Content of the chat message */
  content: string;
}

/**
 * Single recurrence of a webinar
 */
export interface RecurrenceObject {
  id: number;
  webinarId: number;

  frequency: Frequency;

  /** 0-indexed day-of-week starting with sunday (e.g., sunday=0, monday=1, etc.) */
  days: number[];

  start: string;
  end: string;
  timezone: string;
}

/*************
 * API Types *
 *************/

/**
 * Request data to create a new webinar
 */
export interface APIWebinarCreateData
  extends Partial<
    Omit<WebinarObject, "start" | "end" | "days" | "times" | "timezone">
  > {
  /** Name of the webinar */
  name: string;

  /** Type of webinar */
  type: WebinarType;

  /** URL to the webinar video */
  videoUrl: string;

  /** Duration of the webinar in seconds */
  duration: number;

  /** How many days after the webinar the viewer can reply it */
  replayExpiration: number;

  /** Webinar thumbnail */
  thumbnail: string;
}

/**
 * Request data to update a webinar
 */
export interface APIWebinarUpdateData
  extends Partial<
    Omit<APIWebinarCreateData, "videoUrl" | "duration" | "type">
  > {}

export interface APIWebinarAddRecurrenceData
  extends Omit<RecurrenceObject, "id" | "webinarId"> {}

/**
 * Add the video url to a live event
 */
export interface APIWebinarAddEventVideoUrl {
  videoUrl: string;
}

/**
 * Create new notification gateway
 */
export interface APINotificationGatewaySetData {
  provider: IntegrationProvider;
  settings: object;
}

/**
 * Request to create a product offer
 */
export interface APIWebinarOfferCreateData {
  start: number;
  end: number;
  name: string;
  description: string;
  buttonText: string;
  checkout: string;
  price: number;
  thumbnail: string;
}

/**
 * Request to update a product offer
 */
export interface APIWebinarOfferUpdateData
  extends Partial<APIWebinarOfferCreateData> {}

/**
 * Create save chat
 */
export interface APIWebinarChatCreateData
  extends Omit<WebinarChatObject, "id"> {}

/**
 * Update saved chat
 */
export interface APIWebinarChatUpdateData
  extends Omit<WebinarChatObject, "id"> {}

/**
 * Request to register for a webinar
 */
export interface APIWebinarAttendeeCreateData {
  email: string;
  phone?: string;
  firstName: string;
  lastName: string;
  timezone: string;
}
