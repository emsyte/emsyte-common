import { TeamUserRole } from "./team";

const MEMBER_PERMISSIONS = [
  "template:list",
  "template:read",
  "template:create",
  "template:edit",
  "template:activate",
  "template:delete",

  "notification:list",
  "notification:read",
  "notification:create",
  "notification:edit",
  "notification:delete",

  "product-offer:list",
  "product-offer:read",
  "product-offer:create",
  "product-offer:edit",
  "product-offer:delete",

  "webinar:list",
  "webinar:read",

  "members:list",
  "members:read",

  "team:read",
];

const ADMIN_PERMISSIONS = [
  ...MEMBER_PERMISSIONS,

  "webinar:create",
  "webinar:edit",
  "webinar:schedule",
  "webinar:delete",

  "members:invite",
  "members:edit",
  "members:remove",

  "integrations:read",
  "integrations:edit",

  "team:edit",
];

const OWNER_PERMISSIONS = [
  ...ADMIN_PERMISSIONS,
  "team:delete",

  "billing:read",
  "billing:edit",
];

export const PERMISSIONS: Record<TeamUserRole, string[]> = {
  [TeamUserRole.MEMBER]: MEMBER_PERMISSIONS,
  [TeamUserRole.ADMIN]: ADMIN_PERMISSIONS,
  [TeamUserRole.OWNER]: OWNER_PERMISSIONS,
};
