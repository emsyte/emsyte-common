export enum TemplateType {
  /** The page displayed to the viewer after they've finished viewing the webinar */
  THANK_YOU = "thankyou",

  /** The page that's displayed to the audience member after they've registered for the webinar */
  CONFIRMATION = "confirmation",

  /** The page the audience member goes views to register for a webinar */
  REGISTRATION = "registration",
}

export interface TemplateGrapesJSData {
  assets: string;
  components: string;
  css: string;
  html: string;
  styles: string;
}

export interface TemplateObject {
  id: number;
  name: string;
  type: TemplateType;
  thumbnail: string;
  base: boolean;
}

export interface TemplateObjectFull
  extends TemplateObject,
    TemplateGrapesJSData {}

export interface TemplateInstanceObject {
  id: number;
  type: TemplateType;
  thumbnail: string;
  webinarId: number;
  parentId: number;
}

export interface TemplateInstanceObjectFull
  extends Omit<TemplateInstanceObject, "parentId">,
    TemplateGrapesJSData {
  parent: TemplateObject;
}

/*************
 * API Types *
 *************/

/**
 * Create template instance from global template
 */
export interface APITemplateInstanceCreateData {
  parentId: number;
}

export interface APITemplateInstanceUpdateData extends TemplateGrapesJSData {
  thumbnail: string;
}

/**
 * Create global template
 */
export interface APITemplateCreateData {
  name: string;
  instanceId: number;
}

/**
 * Update global template
 */
export interface APITemplateUpdateData {
  name?: string;
  instanceId?: number;
}
