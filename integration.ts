import { UserObject } from "./user";

export enum IntegrationType {
  EMAIL = "email",
  SMS = "sms",
  CRM = "crm",
}

export enum IntegrationProvider {
  SEND_GRID = "sendgrid",
  MAILCHIMP = "mailchimp",
  TWILIO = "twilio",
  ACTIVE_CAMPAIGN = "active_campaign",
  HUB_SPOT = "hub_spot",

  CONSTANT_CONTACT = "constant_contact",
}

export const INTEGRATIONS = [
  {
    provider: IntegrationProvider.SEND_GRID,
    type: IntegrationType.EMAIL,
    title: "Sendgrid",
    description: "World's Largest Cloud-Based Email Delivery Platform.",
  },
  {
    provider: IntegrationProvider.TWILIO,
    type: IntegrationType.SMS,
    title: "Twilio",
    description: "Communication APIs for SMS, Voice, Video and Authentication",
  },
  // {
  //   provider: IntegrationProvider.MAILCHIMP,
  //   type: IntegrationType.CRM,
  //   title: "MailChimp",
  //   description: "All-In-One Integrated Marketing Platform",
  // },
  {
    provider: IntegrationProvider.ACTIVE_CAMPAIGN,
    type: IntegrationType.CRM,
    title: "ActiveCampaign",
    description: "Marketing Automation - Small Business CRM",
  },
  {
    provider: IntegrationProvider.HUB_SPOT,
    type: IntegrationType.CRM,
    title: "HubSpot",
    description: "Inbound Marketing, Sales, and Service Software",
  },

  {
    provider: IntegrationProvider.CONSTANT_CONTACT,
    type: IntegrationType.CRM,
    title: "Constant Contact",
    description: "Powerful email. No restrictions. Real results.",
  },
];

export interface IntegrationObject {
  user?: UserObject;

  type: IntegrationType;
  provider: IntegrationProvider;
  settings: { [key: string]: string };
}

export interface IntegrationObjectFull extends IntegrationObject {
  title: string;
  description: string;

  active?: boolean;
}

export interface APIIntegrationSetData {
  provider: IntegrationProvider;
  settings: { [key: string]: string };

  active?: boolean;
}
